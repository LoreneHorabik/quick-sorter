Java implementation of [quick sort](https://en.wikipedia.org/wiki/Quicksort). This program will put less load of CPU and memory will take all the load.

* First make one variable of the [array pivot](http://www.w3schools.com/js/js_arrays.asp) (Call it a pivot element)
* Now you have to compare elements and shift elements with less values to before the index position of the pivot. While remaining elements will have to shift to after the [index position](https://en.wikipedia.org/wiki/Sorting). This is divide and conquer operation.
* Now perform step 2 over and over with smaller sub-Arrays. In order to get more information visit [our website](www.youtube.com/watch?v=kPRA0W1kECg) [here](http://99onlinepoker.co). 

* More information
- https://github.com/Micckael/Sort
- https://github.com/mrmikemcguire/Quick-Sorter
- https://github.com/creeveshft/Quicksort_python_stock